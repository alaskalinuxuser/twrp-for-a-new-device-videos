# HD TWRP For A New Device

# About
I get a lot of questions about how to compile Android and how to make custom kernels, so I thought I would put together a video series with everything I know. (Don't worry, that will not take long!)

# Who is this course for?
This course is for those who are able to flash custom recoveries, like TRWP or CWM, and who can root their phones or flash custom roms. Now that you can do those things, you are ready to start building your very own custom roms and kernels!

# What do we cover?
In this series, we are covering the process of creating a device tree from scratch for TWRP. The example phone is a BLU Life XL, which (to my knowledge, and at this time) does not have a device tree, a TWRP recovery image, or custom rom.

